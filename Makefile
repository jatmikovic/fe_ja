DATABASE_NAME = nyobi-fe-ja

run: .env create bin/nyobi-fe-ja
	@PATH="$(PWD)/bin:$(PATH)" heroku local

.env:
	cp .env.dev .env

bin/nyobi-fe-ja: main.go
	go build -o bin/nyobi-fe-ja main.go

create:
	@psql -lqt | cut -d \| -f 1 | grep -qw $(DATABASE_NAME) || createdb $(DATABASE_NAME)

drop:
	dropdb $(DATABASE_NAME)

clean:
	rm -rf bin

