CREATE TABLE users (
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  username STRING DEFAULT NULL,
  first_name STRING  NOT NULL,
  last_name STRING  NOT NULL,
  password STRING DEFAULT NULL,
  address  STRING  NOT NULL
);
